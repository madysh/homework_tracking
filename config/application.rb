require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module HomeworkTracking
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # TODO: remove, when start work optional: true options
    config.active_record.belongs_to_required_by_default = false
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    mailer_options = Rails.application.secrets.mailer
    config.action_mailer.delivery_method = :sendmail
    config.action_mailer.default_url_options = mailer_options
      .slice(:host, :port)
    config.action_mailer.default_options = mailer_options
  end
end
