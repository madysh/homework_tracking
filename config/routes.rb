Rails.application.routes.draw do
  devise_for :users

  resources :families do
    resources :family_users, except: :index
    resources :categories, except: :index
    resources :users, only: %i[edit update]
    resources :homeworks do
      resources :user_homeworks, except: %i[index new]
    end
    get 'homeworks/:id/move_to_opened',
      to: 'homeworks#move_to_opened',
      as: :homework_move_to_opened
  end

  resources :user_homeworks, only: :index do
    resources :estimates,
      only: %i[new create],
      controller: :user_homework_estimates
    get 'estimate_consider', to: 'user_homework_estimates#consider'
    post 'estimate_decision', to: 'user_homework_estimates#decision'
    resources :requests,
      only: %i[new create],
      controller: :user_homework_requests
    resources :request_end,
      only: %i[new create],
      controller: :user_homework_request_ends
    resources :request_answer,
      only: %i[new create],
      controller: :user_homework_request_answers
    resources :request_answer_to_end,
      only: %i[new create],
      controller: :user_homework_request_answer_to_ends
  end

  resources :user_homeworks, only: [] do
    resources :ratings, only: %i[create update destroy]
  end

  resources :homeworks, only: [] do
    resources :comments, only: %i[new create] do
      resources :comments, only: :create
    end
  end

  root to: 'families#index'
end
