$ ->
  $('.users-rating').raty
    path: '/assets/ratings',
    cancel: true,
    cancelPlace: 'right',
    score: $(@).data('score')

    click: (score, event) ->
      userHomeworkId = $(@).data('user-homework-id')
      ratingId = $(@).attr('data-rating-id')
      url = '/user_homeworks/' + userHomeworkId + '/ratings'
      if ratingId
        type = if (score == null) then 'DELETE' else 'PATCH'
        url += '/' + ratingId
      else
        type = 'POST'

      $.ajax
        url: url,
        type: type,
        data: { score: score }
        success: ->
          if score == null
            $('.users-rating').removeAttr('data-rating-id')
