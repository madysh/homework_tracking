$ ->
  $homeworksListcontainer = $('.homeworks-list-container')
  $membersList = $('.family-members-list')
  $homeworksContainer = $('.homeworks-container')
  $dragedHomework = null

  $homeworksListcontainer.on 'drag', '.homework-item', (e) ->
    $dragedHomework = $(e.target)
    $membersList.show()
    $homeworksContainer .hide()

  $homeworksListcontainer.on 'dragend', '.homework-item', ->
    $dragedHomework = null
    $membersList.hide()
    $homeworksContainer .show()

  $('.family-members-item').on 'drop', (e) ->
    e.preventDefault()
    target = e.target.closest('.family-members-item')

    if confirm($(target).data('confimation-message'))
      homeworkId = $dragedHomework.data('homework-id')
      familyId = $dragedHomework.data('family-id')
      userId = $(target).data('user-id')
      assignHomework(homeworkId, familyId, userId)


  $('.family-members-item').on 'dragover', (e) ->
    e.preventDefault()
    false

  assignHomework = (homeworkId, familyId, userId) ->
    url = '/families/' + familyId + '/homeworks/' + homeworkId + '/user_homeworks'

    $.ajax
      url: url,
      type: 'POST',
      data: { 'user_homework': { 'user_id': userId } }


