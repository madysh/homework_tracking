$ ->
  $cpmmentsList = $('.comments-list-container')

  $cpmmentsList.on 'click', '.cancle-button', ->
    $(@).closest('.reply-form-container').hide()
    $(@).closest('.comment-container').find('.comment-reply-button').show()

  $cpmmentsList.on 'click', '.comment-reply-button', ->
    $(@).hide()
    $(@).closest('.comment-container').find('.reply-form-container').show()

