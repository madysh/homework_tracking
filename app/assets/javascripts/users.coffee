$ ->
  $('.date-of-birth-datepicker').datetimepicker
    format: 'DD/MM/YYYY'
    showClear: true
    showClose: true
    minDate: new Date(1920, 1, 1)
    maxDate: new Date(2012, 12, 31)
    useCurrent: false
