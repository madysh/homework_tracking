$ ->
  $('.user-homework-request-answers-form').on 'change', 'input[type=radio]', (e) ->
    $container = $('.estimate-container')
    $input = $container.find('input')
    if $(e.target).val() == 'estimate'
      $input.removeAttr('disabled')
      $container.show()
    else
      $input.attr('disabled', 'disabled')
      $container.hide()
