$ ->
  $('.estimate-datetimepicker').datetimepicker
    showClear: true
    showClose: true
    minDate: moment()
    maxDate: moment().add(1, 'year')
    useCurrent: false
