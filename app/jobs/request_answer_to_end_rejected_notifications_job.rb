class RequestAnswerToEndRejectedNotificationsJob < ActiveJob::Base
  queue_as :request_anwer_rejected

  def perform(user_homework, comment)
    UserHomeworkMailer
      .you_rejected_users_request_to_end_email(user_homework, comment)
      .deliver
    UserHomeworkMailer
      .your_request_to_end_rejected_email(user_homework, comment)
      .deliver
  end
end
