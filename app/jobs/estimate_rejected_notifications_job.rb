class EstimateRejectedNotificationsJob < ActiveJob::Base
  queue_as :estimate_aproved

  def perform(user_homework, comment)
    UserHomeworkMailer
      .your_estimate_rejected_email(user_homework, comment)
      .deliver
    UserHomeworkMailer
      .you_rejected_users_estimate_email(user_homework, comment)
      .deliver
  end
end
