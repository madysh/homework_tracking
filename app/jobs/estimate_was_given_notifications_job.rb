class EstimateWasGivenNotificationsJob < ActiveJob::Base
  queue_as :estimate_request

  def perform(user_homework, comment_id)
    UserHomeworkMailer
      .you_estimateg_homework_email(user_homework, comment_id)
      .deliver
    UserHomeworkMailer
      .user_estimated_homework_email(user_homework, comment_id)
      .deliver
  end
end
