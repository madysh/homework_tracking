class UserRequestEndNotificationsJob < ActiveJob::Base
  queue_as :estimate_request

  def perform(user_homework, comment)
    UserHomeworkMailer
      .you_created_request_to_end_email(user_homework, comment)
      .deliver
    UserHomeworkMailer
      .user_created_request_to_end_email(user_homework, comment)
      .deliver
  end
end
