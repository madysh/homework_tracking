class MovedToOpenedNotificationsJob < ActiveJob::Base
  queue_as :moved_to_opened

  def perform(user_homework)
    UserHomeworkMailer
      .assigned_to_you_homework_was_returned_to_opened_email(user_homework)
      .deliver
    UserHomeworkMailer
      .you_returned_homework_to_opened_email(user_homework)
      .deliver
  end
end
