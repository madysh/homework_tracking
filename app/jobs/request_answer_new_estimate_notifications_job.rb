class RequestAnswerNewEstimateNotificationsJob < ActiveJob::Base
  queue_as :request_anwer_rejected

  def perform(user_homework, comment)
    UserHomeworkMailer
      .you_updated_users_estimate_email(user_homework, comment)
      .deliver
    UserHomeworkMailer
      .your_estimate_was_changed_email(user_homework, comment)
      .deliver
  end
end
