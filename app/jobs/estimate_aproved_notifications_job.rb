class EstimateAprovedNotificationsJob < ActiveJob::Base
  queue_as :estimate_aproved

  def perform(user_homework, comment)
    UserHomeworkMailer
      .your_estimate_aproved_email(user_homework, comment)
      .deliver
    UserHomeworkMailer
      .you_aproved_users_estimate_email(user_homework, comment)
      .deliver
  end
end
