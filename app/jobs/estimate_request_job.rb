class EstimateRequestJob < ActiveJob::Base
  queue_as :estimate_request

  def perform(user_homework)
    UserHomeworkMailer
      .to_you_was_assigned_a_homework_email(user_homework)
      .deliver
    UserHomeworkMailer.you_assigned_a_homework_email(user_homework).deliver
  end
end
