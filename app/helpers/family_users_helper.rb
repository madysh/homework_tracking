module FamilyUsersHelper
  def famy_user_users_options(family)
    family_members_ids = family.users.ids
    User.where.not(id: family_members_ids).decorate.map do |user|
      [user.name, user.id]
    end
  end

  def famy_user_roles_options
    FamilyUser.roles.keys.map { |role| [t("family_users.form.#{role}"), role] }
  end
end
