module HomeworksHelper
  def options_for_homeworks_scope
    Homework.scopes.keys.map { |scope| [t("general.scope.#{scope}"), scope] }
  end
end
