module CategoriesHelper
  def options_for_category_id(family, filter_by_depth: false)
    roots = family.categories.roots.ordered_by_name
    roots.each_with_object([]) do |category, res|
      res.concat category_and_subcategories_options(category, filter_by_depth)
    end
  end

  def options_for_categories_scope
    Category.scopes.keys.map { |scope| [t("general.scope.#{scope}"), scope] }
  end

  private

  def category_options(category)
    option_name = ('&nbsp;&nbsp;' * (category.depth - 1)) + category.name
    [option_name.html_safe, category.id]
  end

  def category_and_subcategories_options(category, filter_by_depth)
    cat_options = [category_options(category)]
    if filter_by_depth && (category.depth == Category::MAX_LEVEL - 1)
      cat_options
    else
      self_and_subcategories_options(category, filter_by_depth, cat_options)
    end
  end

  def self_and_subcategories_options(category, filter_by_depth, cat_options)
    children = category.children.ordered_by_name
    if children.any?
      children.each_with_object(cat_options) do |subcat, array|
        array
          .concat category_and_subcategories_options(subcat, filter_by_depth)
      end
    else
      cat_options
    end
  end
end
