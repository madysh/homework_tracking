module ApplicationHelper
  def users_avatar_img(user)
    user = user.decorate
    object_image_tag(user, user.avatar_path)
  end

  def family_logo_img(family)
    family = family.decorate
    object_image_tag(family, family.logo_path)
  end

  private

  def object_image_tag(object, iamge_path)
    image_tag iamge_path,
      title: object.name,
      class: 'img-circle avatar-or-logo',
      draggable: false
  end
end
