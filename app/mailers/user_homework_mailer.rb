class UserHomeworkMailer < ApplicationMailer
  def you_assigned_a_homework_email(user_homework)
    define_variables_for_who_assigned(user_homework)
    send_mail(__method__)
  end

  def you_returned_homework_to_opened_email(user_homework)
    define_variables_for_who_assigned(user_homework)
    send_mail(__method__)
  end

  def user_estimated_homework_email(user_homework, comment_id)
    define_variables_for_who_assigned(user_homework)
    @comment_id = comment_id
    send_mail(__method__)
  end

  def you_aproved_users_estimate_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_rejected_users_estimate_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def user_created_request_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def user_created_request_to_end_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_rejected_users_request_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_rejected_users_request_to_end_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_applyed_users_request_to_end_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_updated_users_estimate_email(user_homework, comment)
    define_variables_for_who_assigned(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_estimateg_homework_email(user_homework, comment_id)
    define_variables_for_assigned_user(user_homework)
    @comment_id = comment_id
    send_mail(__method__)
  end

  def to_you_was_assigned_a_homework_email(user_homework)
    define_variables_for_assigned_user(user_homework)
    send_mail(__method__)
  end

  def assigned_to_you_homework_was_returned_to_opened_email(user_homework)
    define_variables_for_assigned_user(user_homework)
    send_mail(__method__)
  end

  def your_estimate_aproved_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def your_estimate_rejected_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_created_request_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def you_created_request_to_end_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def your_request_rejected_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def your_request_to_end_rejected_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def your_request_to_end_applyed_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  def your_estimate_was_changed_email(user_homework, comment)
    define_variables_for_assigned_user(user_homework)
    @comment = comment
    send_mail(__method__)
  end

  private

  def define_variables_for_assigned_user(user_homework)
    @user = user_homework.user.decorate
    @assigned_by = user_homework.assigned_by_user.decorate
    define_common_variables(user_homework)
  end

  def define_variables_for_who_assigned(user_homework)
    @user = user_homework.assigned_by_user.decorate
    @assigned_to = user_homework.user.decorate
    define_common_variables(user_homework)
  end

  def define_common_variables(user_homework)
    @homework = user_homework.homework
    @family = @homework.family
  end

  def send_mail(action_name)
    mail(
      to: @user.email,
      subject: t("user_homework_mailer.#{action_name}.subject"),
      content_type: 'text/html'
    )
  end
end
