class FamilyUserDecorator < Draper::Decorator
  delegate_all

  def user_name
    user.decorate.name
  end
end
