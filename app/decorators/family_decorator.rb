class FamilyDecorator < Draper::Decorator
  delegate_all

  def logo_path
    logo_url || 'default-family-logo.jpeg'
  end
end
