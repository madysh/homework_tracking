class UserDecorator < Draper::Decorator
  delegate_all

  def name
    nickname || [first_name, last_name].join(' ')
  end

  def avatar_path
    avatar_url || 'default-avatar.jpg'
  end
end
