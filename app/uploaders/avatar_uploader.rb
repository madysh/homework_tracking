class AvatarUploader < BaseUploader
  def filename
    'avatar.jpg' if original_filename
  end
end
