class FamilyLogoUploader < BaseUploader
  def filename
    'logo.jpg' if original_filename
  end
end
