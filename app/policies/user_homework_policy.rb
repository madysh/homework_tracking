class UserHomeworkPolicy
  attr_reader :user, :user_homework, :homework, :family

  def initialize(user, user_homework)
    @user = user
    @user_homework = user_homework
    @homework = @user_homework.homework
    @family = @user_homework.homework.family
  end

  def create?
    user && homework.opened? && family.member?(user)
  end

  def rate_user?
    user &&
      (homework.ended? || homework.closed?) &&
      (user_homework.user != user) &&
      (user_homework.assigned_by_user == user)
  end
end
