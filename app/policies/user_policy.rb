class UserPolicy
  attr_reader :user, :current_user

  def initialize(current_user, user)
    @user = user
    @current_user = current_user
  end

  def update?(family = nil)
    (user == current_user) || family&.parent?(current_user)
  end

  alias edit? update?
end
