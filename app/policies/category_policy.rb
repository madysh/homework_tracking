class CategoryPolicy
  attr_reader :user, :category, :family

  def initialize(user, category)
    @user = user
    @category = category
    @family = @category.family
  end

  def create?
    user && family.member?(user)
  end

  alias new? create?

  def update?
    create? && family.parent?(user)
  end

  alias edit? update?
  alias destroy? update?

  def create_subcategory?
    create? && (category.depth < Category::MAX_LEVEL)
  end
end
