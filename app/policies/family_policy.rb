class FamilyPolicy
  attr_reader :user, :family

  def initialize(user, family)
    @user = user
    @family = family
  end

  def update?
    user && family.parent?(user)
  end

  alias create_family_user? update?
  alias edit? update?

  def edit_user?(passed_user)
    update? && user != passed_user
  end

  def create?
    user
  end

  alias new? create?

  def create_category?
    user && family.member?(user)
  end

  alias show_private_categories? create_category?
  alias show_private_homeworks? create_category?
  alias create_homework? create_category?

  def destroy?
    user && family.owner_id == user.id
  end

  def show?
    true
  end

  def assign_homework?
    user && @family.member?(user)
  end
end
