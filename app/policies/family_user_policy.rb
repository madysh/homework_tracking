class FamilyUserPolicy
  attr_reader :user, :family_user, :family

  def initialize(user, family_user)
    @user = user
    @family_user = family_user
    @family = family_user.family
  end

  def create?
    user && family.parent?(user)
  end

  def destroy?
    create? && (family.owner?(family_user.user) ? false : true)
  end

  def update?
    create? && family_user.user != user
  end

  alias edit? update?
  alias new? create?

  def show?
    user
  end
end
