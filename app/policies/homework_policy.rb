class HomeworkPolicy
  attr_reader :user, :homework, :family

  def initialize(user, homework)
    @user = user
    @homework = homework
    @family = @homework.family
  end

  def create?
    user && family.member?(user)
  end

  alias new? create?
  alias show? create?
  alias create_comment? create?

  def update?
    create? && (family.parent?(user) || homework.user == user)
  end

  alias edit? update?
  alias destroy? update?

  def assign?
    create? && homework.opened?
  end

  def move_to_opened?
    user && !homework.opened? && family.parent?(user)
  end

  def estimate?
    user &&
      homework.assigned? &&
      homework.user_homework.estimate.nil? &&
      (homework.user_homework.user == user)
  end

  def approve_estimate?
    user &&
      homework.assigned? &&
      homework.user_homework.estimate? &&
      (homework.user_homework.assigned_by_id == user.id)
  end

  def pause?
    user &&
      homework.started? &&
      (homework.user_homework.user == user)
  end

  def end?
    user &&
      (homework.started? || homework.paused?) &&
      (homework.user_homework.user == user)
  end

  def answer_to_request?
    user &&
      (homework.paused? || homework.ended?) &&
      (homework.user_homework.assigned_by_id == user.id)
  end
end
