class RatingPolicy
  attr_reader :user, :rating, :user_homework, :homework

  def initialize(user, rating)
    @user = user
    @rating = rating
    @user_homework = @rating.user_homework
    @homework = @user_homework.homework
  end

  def create?
    user &&
      (homework.ended? || homework.closed?) &&
      (user_homework.user != user) &&
      (user_homework.assigned_by_user == user)
  end

  alias update? create?
  alias destroy? create?
end
