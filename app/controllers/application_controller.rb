class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_users_language
  layout :layout_by_resource

  def current_user
    super&.decorate
  end

  private

  def after_inactive_sign_up_path_for(*)
    new_user_session_path
  end

  def set_users_language
    I18n.locale = current_user.system_language if signed_in?
  end

  def layout_by_resource
    devise_controller? ? 'simple' : 'application'
  end

  def configure_permitted_parameters
    users_attrs = %i[
      first_name last_name nickname email date_of_birth address system_language
      password password_confirmation current_password avatar
    ]
    devise_parameter_sanitizer.permit :sign_up, keys: users_attrs
    devise_parameter_sanitizer.permit :account_update, keys: users_attrs
    devise_parameter_sanitizer.permit :sign_in,
      keys: %i[email password remember_me]
  end
end
