class HomeworksController < ApplicationController
  before_action :set_family
  before_action :set_homework, only: %i[show edit update move_to_opened]
  respond_to :json, only: :move_to_opened

  def index
    set_categories
    set_categories_with_homeworks
    set_homeworks
  end

  def show
    @comments = @homework.comments
  end

  def new
    @homework = @family
      .homeworks
      .where(user: current_user, category_id: params[:category_id])
      .build
    authorize @homework
  end

  def create
    @homework = @family
      .homeworks
      .where(user_id: current_user.id)
      .build(homework_params)
    authorize @homework
    if @homework.save
      redirect_to family_homeworks_path(@family)
    else
      render :new
    end
  end

  def update
    if @homework.update(homework_params)
      redirect_to family_homeworks_path(@family)
    else
      render :edit
    end
  end

  def destroy
    @homework = @family.homeworks.find(params[:id])
    authorize @homework
    @homework.destroy
    redirect_to family_homeworks_path(@family)
  end

  def move_to_opened
    @homework.move_to_opened!
    MovedToOpenedNotificationsJob.perform_later(@homework.user_homework)
  end

  private

  def homework_params
    params
      .require(:homework)
      .permit(:name, :user_id, :description, :category_id)
  end

  def set_family
    @family = Family.find(params[:family_id])
  end

  def set_categories
    @categories = @family.categories.ordered_by_name
    return if policy(@family).show_private_categories?

    @categories = @categories.as_public
  end

  def set_categories_with_homeworks
    @categories_with_homeworks = @categories
      .joins(:homeworks)
      .select(:name, :id, 'homeworks.state')
      .group_by do |category|
        [category.state, category.id]
      end
  end

  def set_homeworks
    @homeworks = @family.homeworks.ordered_by_name
    unless policy(@family).show_private_homeworks?
      @homeworks = @homeworks.as_public
    end
    groupe_homeworks
  end

  def set_homework
    @homework = @family.homeworks.find(params[:id])
    authorize @homework
  end

  def groupe_homeworks
    @homeworks = @homeworks
      .group_by(&:state)
      .transform_values do |homework|
        homework.group_by(&:category_id)
      end
  end
end
