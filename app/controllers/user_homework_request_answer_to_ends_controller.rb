class UserHomeworkRequestAnswerToEndsController < UserHomeworkRequestAnswersController
  private

  def handle_answer(comment)
    super
    apply_request_answer(comment) if params[:answer] == 'apply'
  end

  def reject_request_answer(comment)
    @homework.start!
    RequestAnswerToEndRejectedNotificationsJob
      .perform_later @user_homework, comment
  end

  def apply_request_answer(comment)
    @homework.close!
    RequestAnswerToEndApplyedNotificationsJob
      .perform_later @user_homework, comment
  end

  def authorize_homework
    authorize @homework, :answer_to_request?
  end
end
