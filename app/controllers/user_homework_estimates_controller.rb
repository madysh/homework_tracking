class UserHomeworkEstimatesController < ApplicationController
  include UserHomeworksResource
  include Estimateble

  before_action :authorize_homework_to_estimate, only: %i[new create]
  before_action :authorize_homework_to_approve_estimate,
    only: %i[consider decision]
  before_action :set_family
  before_action :find_comment, only: %i[consider decision]
  before_action :set_new_estimate_to_user_homework, only: :create

  def create
    if params[:comment] && @user_homework.save
      comment = create_comment(@homework)
      EstimateWasGivenNotificationsJob
        .perform_later(@user_homework, comment.id)
      redirect_to family_homeworks_path(@family)
    else
      render :new
    end
  end

  def decision
    render :consider unless params[:comment] || params[:aproved]

    comment = create_comment(@comment)
    if params[:aproved] == 'yes'
      @homework.start!
      EstimateAprovedNotificationsJob.perform_later @user_homework, comment
    else
      @homework.user_homework.clear_estimate
      EstimateRejectedNotificationsJob.perform_later @user_homework, comment
    end

    redirect_to family_homeworks_path(@family)
  end

  private

  def user_homework_params
    params.require(:user_homework).permit(:estimate)
  end

  def authorize_homework_to_estimate
    authorize @homework, :estimate?
  end

  def authorize_homework_to_approve_estimate
    authorize @homework, :approve_estimate?
  end
end
