class UserHomeworksController < ApplicationController
  before_action :set_family, except: :index
  before_action :set_homework, only: :create
  before_action :set_user_homework_to_create, only: :create
  respond_to :json, only: :create

  def index
    @homeworks = current_user_assigned_homeworks
  end

  def create
    return unless @user_homework.save
    @homework.assign!
    EstimateRequestJob.perform_later(@user_homework)
  end

  private

  def set_user_homework_to_create
    @user_homework = @homework.build_user_homework(homework_params)
    @user_homework.assigned_by_id = current_user.id
    authorize @user_homework
  end

  def homework_params
    params
      .require(:user_homework)
      .permit(:user_id, :homework_id, :assigned_by_id)
  end

  def set_family
    @family = Family.find(params[:family_id])
  end

  def set_homework
    @homework = @family.homeworks.find(params[:homework_id])
  end

  def current_user_assigned_homeworks
    homeworks = current_user.assigned_homeworks.joins(:category, :family)
    homeworks = select_columns(homeworks)
    handle_to_hash(homeworks)
  end

  def select_columns(records)
    records.select(
      'homeworks.*, categories.name category_name,
      families.id family_id, families.name family_name'
    )
  end

  # handle to array of hashes:
  # [
  #   [family_id, family_name]: [
  #     [category_id, category_name] : [Homework,...]
  #   ]
  # ]
  def handle_to_hash(records)
    records
      .group_by { |homework| [homework.family_id, homework.family_name] }
      .transform_values do |value|
        value.group_by do |homework|
          [homework.category_id, homework.category_name]
        end
      end
  end
end
