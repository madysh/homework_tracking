class CategoriesController < ApplicationController
  before_action :set_family
  before_action :set_category, only: %i[edit update]

  def new
    @category = @family.categories.build(parent_id: params[:category_id])
    authorize @category
  end

  def create
    @category = @family.categories.build(category_params)
    authorize @category
    if @category.save
      redirect_to family_homeworks_path(@family)
    else
      render :new
    end
  end

  def update
    if @category.update(category_params)
      redirect_to family_homeworks_path(@family)
    else
      render :edit
    end
  end

  def destroy
    @category = @family.categories.find(params[:id])
    authorize @category
    @category.destroy
    redirect_to family_homeworks_path(@family)
  end

  private

  def category_params
    params.require(:category).permit(:name, :scope, :parent_id)
  end

  def set_category
    @category = @family.categories.find(params[:id])
    authorize @category
  end

  def set_family
    @family = Family.find(params[:family_id])
  end
end
