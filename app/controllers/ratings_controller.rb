class RatingsController < ApplicationController
  before_action :set_user_homework
  before_action :find_and_authorize_rating, only: %i[update destroy]
  after_action :recount_average_rating
  respond_to :json

  def create
    @rating = @user_homework.build_rating(score: params[:score])
    authorize @rating
    @rating.save
  end

  def update
    @rating.score = params[:score]
    @rating.save
  end

  def destroy
    @rating.really_destroy!
  end

  private

  def recount_average_rating
    @user_homework.user.recount_average_rating
  end

  def set_user_homework
    @user_homework = UserHomework.find(params[:user_homework_id])
  end

  def find_and_authorize_rating
    @rating = @user_homework.rating
    authorize @rating
  end
end
