class UserHomeworkRequestAnswersController < ApplicationController
  include UserHomeworksResource
  include Estimateble
  before_action :authorize_homework
  before_action :set_family
  before_action :find_comment, only: :new

  def create
    if params[:comment] && params[:answer]
      comment = create_comment(@homework)
      handle_answer(comment)
      redirect_to family_homeworks_path(@family)
    else
      render :new
    end
  end

  private

  def handle_answer(comment)
    if params[:answer] == 'estimate'
      new_estimate_request_answer(comment)
    elsif params[:answer] == 'move_to_opened'
      move_to_opened_request_answer
    elsif params[:answer] == 'reject'
      reject_request_answer(comment)
    end
  end

  def new_estimate_request_answer(comment)
    @homework.start!
    set_new_estimate_to_user_homework(save: true)
    RequestAnswerNewEstimateNotificationsJob
      .perform_later(@user_homework, comment)
  end

  def move_to_opened_request_answer
    @homework.move_to_opened!
    MovedToOpenedNotificationsJob.perform_later @user_homework
  end

  def reject_request_answer(comment)
    @homework.start!
    RequestAnswerRejectedNotificationsJob.perform_later @user_homework, comment
  end

  def authorize_homework
    authorize @homework, :answer_to_request?
  end
end
