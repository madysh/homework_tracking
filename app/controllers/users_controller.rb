class UsersController < ApplicationController
  before_action :set_family
  before_action :set_user
  before_action :check_permissions

  def update
    if @user.update(user_params)
      redirect_to @family
    else
      render :edit
    end
  end

  private

  def user_params
    params
      .require(:user)
      .permit %i[
        first_name last_name nickname email date_of_birth address
        system_language avatar
      ]
  end

  def set_family
    @family = Family.find(params[:family_id])
  end

  def set_user
    @user = @family.users.find(params[:id])
  end

  def check_permissions
    raise Pundit::NotAuthorizedError unless policy(@family).edit_user?(@user)
  end
end
