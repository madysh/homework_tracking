class FamilyUsersController < ApplicationController
  before_action :set_family
  before_action :set_family_user, only: %i[edit update show destroy]

  def new
    @family_user = @family.family_users.build
    authorize @family_user
  end

  def create
    @family_user = @family.family_users.build(family_user_params)
    authorize @family_user
    if @family_user.save
      redirect_to @family
    else
      render :new
    end
  end

  def update
    if @family_user.update(family_user_params)
      redirect_to @family
    else
      render :edit
    end
  end

  def destroy
    @family_user.destroy
    redirect_to family_path(@family)
  end

  private

  def family_user_params
    params.require(:family_user).permit(:user_id, :role)
  end

  def set_family
    @family = Family.find(params[:family_id]).decorate
    authorize @family
  end

  def set_family_user
    @family_user = @family.family_users.find(params[:id]).decorate
    authorize @family_user
  end
end
