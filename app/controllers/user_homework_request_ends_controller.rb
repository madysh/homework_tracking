class UserHomeworkRequestEndsController < ApplicationController
  include UserHomeworksResource
  before_action :authorize_homework
  before_action :set_family

  def create
    if params[:comment]
      comment = create_comment(@homework)
      @homework.end!
      UserRequestEndNotificationsJob.perform_later(@user_homework, comment)
      redirect_to family_homeworks_path(@family)
    else
      render :new
    end
  end

  private

  def authorize_homework
    authorize @homework, :end?
  end
end
