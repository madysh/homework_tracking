module Estimateble
  extend ActiveSupport::Concern
  ESTIMATE_FORMAT = '%m/%d/%Y %l:%M %p'

  private

  def user_homework_params
    params.require(:user_homework).permit(:estimate)
  end

  def estimate_as_time_object
    Time.strptime(user_homework_params[:estimate], ESTIMATE_FORMAT)
  end

  def set_new_estimate_to_user_homework(save: false)
    @user_homework.estimate = estimate_as_time_object
    @user_homework.save if save
  end
end
