module UserHomeworksResource
  extend ActiveSupport::Concern

  included do
    before_action :set_user_homework
    before_action :set_homework
  end

  private

  def set_user_homework
    @user_homework = UserHomework.find(params[:user_homework_id])
  end

  def set_homework
    @homework = @user_homework.homework
  end

  def set_family
    @family = @homework.family
  end

  def create_comment(object)
    object.comments.create(user: current_user, body: params[:comment])
  end

  def find_comment
    @comment = @homework.comments.find(params[:comment_id])
  end
end
