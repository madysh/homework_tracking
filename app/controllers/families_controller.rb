class FamiliesController < ApplicationController
  before_action :set_family, only: %i[show edit update destroy]

  def index
    @families = Family.includes(:family_users).decorate
  end

  def show
    @family_users = @family.family_users.group_by(&:role)
  end

  def new
    @family = Family.new.decorate
    authorize @family
  end

  def create
    @family = Family.new(family_params)
    authorize @family
    @family.owner_id = current_user.id

    if @family.save
      @family.add_parent(current_user)
      redirect_to @family
    else
      render :new
    end
  end

  def update
    if @family.update(family_params)
      redirect_to @family
    else
      render :edit
    end
  end

  def destroy
    @family.destroy
    redirect_to families_path
  end

  private

  def set_family
    @family = Family.find(params[:id]).decorate
    authorize @family
  end

  def family_params
    params.require(:family).permit(:name, :logo)
  end
end
