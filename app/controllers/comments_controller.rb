class CommentsController < ApplicationController
  before_action :set_homework
  before_action :set_level
  respond_to :json

  def create
    if params[:comment_id]
      define_comments_scope_by_comment_id
    else
      define_comments_scope
    end
    @comment = @comments.create(comment_params)
  end

  private

  def set_homework
    @homework = Homework.find(params[:homework_id])
  end

  def set_level
    @level = params[:comment][:level].to_i
  end

  def define_comments_scope_by_comment_id
    @parent_comment = Comment.find(params[:comment_id])
    @comments = @parent_comment.comments.where(user: current_user)
  end

  def define_comments_scope
    @comments = @homework.comments.where(user: current_user)
  end

  def comment_params
    params.require(:comment).permit(:body)
  end
end
