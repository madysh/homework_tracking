class Comment < ApplicationRecord
  include ActsAsCommentable::Comment

  MAX_LENGTH = 256
  MAX_NESTING_LEVEL = 3

  belongs_to :commentable, polymorphic: true
  belongs_to :user

  acts_as_commentable
  auto_strip_attributes :body

  validates :user_id, :commentable_type, :commentable_id,
    presence: true
  validates :body, presence: true, length: { maximum: MAX_LENGTH }
end
