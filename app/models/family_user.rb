class FamilyUser < ApplicationRecord
  PARENT_ROLE_NAME = :a_parent
  CHILD_ROLE_NAME = :a_child
  belongs_to :user
  belongs_to :family

  scope :all_parents, -> { where(role: PARENT_ROLE_NAME) }
  scope :all_children, -> { where(role: CHILD_ROLE_NAME) }

  enum role: [PARENT_ROLE_NAME, CHILD_ROLE_NAME]
end
