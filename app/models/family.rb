class Family < ApplicationRecord
  acts_as_paranoid

  mount_uploader :logo, FamilyLogoUploader

  belongs_to :owner, class_name: 'User', foreign_key: :owner_id
  has_many :family_users, dependent: :destroy
  has_many :family_users_parents,
    -> { FamilyUser.all_parents },
    class_name: 'FamilyUser'
  has_many :family_users_children,
    -> { FamilyUser.all_children },
    class_name: 'FamilyUser'
  has_many :users, through: :family_users, dependent: :destroy
  has_many :all_parents, through: :family_users_parents, source: :user
  has_many :all_children, through: :family_users_children, source: :user
  has_many :categories, dependent: :destroy
  has_many :homeworks, dependent: :destroy

  validates :name, :owner_id, presence: true

  auto_strip_attributes :name

  def add_parent(user)
    family_users_parents.create(user: user)
  end

  def owner?(user)
    owner_id == user.id
  end

  def parent?(user)
    all_parents.include?(user)
  end

  def member?(user)
    users.include?(user)
  end
end
