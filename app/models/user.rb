class User < ApplicationRecord
  PASSWORD_MIN_LENGTH = 6
  PASSWORD_MAX_LENGTH = 128

  acts_as_paranoid

  has_many :family_users, dependent: :destroy
  has_many :families, through: :family_users, dependent: :destroy
  has_many :homeworks, dependent: :destroy
  has_many :user_homeworks, dependent: :destroy
  has_many :ratings, through: :user_homeworks, dependent: :destroy
  has_many :assigned_homeworks,
    dependent: :destroy,
    through: :user_homeworks,
    source: :homework

  enum system_language: %i[en ru]

  devise :database_authenticatable, :registerable, :rememberable,
    :trackable, :confirmable, :recoverable

  auto_strip_attributes :first_name, :last_name, :email, :nickname, :address

  default_scope -> { where('confirmed_at IS NOT NULL') }

  validates :password,
    confirmation: true,
    allow_blank: true,
    length: { minimum: PASSWORD_MIN_LENGTH, maximum: PASSWORD_MAX_LENGTH }
  validates :password, presence: true, on: :create
  validates :email,
    presence: true,
    uniqueness: true,
    format: { with: Devise.email_regexp },
    allow_blank: true
  validates :nickname, uniqueness: true, allow_blank: true
  validates :first_name, :system_language, presence: true

  mount_uploader :avatar, AvatarUploader

  def recount_average_rating
    self.average_rating = actual_average_rating
    save
  end

  private

  def actual_average_rating
    if ratings.empty?
      0
    else
      ratings_sum = ratings.pluck(:score).inject(&:+)
      ratings_sum.to_f / ratings.count
    end
  end
end
