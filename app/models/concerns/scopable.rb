module Scopable
  extend ActiveSupport::Concern

  included do
    enum scope: %i[as_public as_private]
  end
end
