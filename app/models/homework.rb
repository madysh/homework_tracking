class Homework < ApplicationRecord
  include Scopable
  include AASM

  STATE_OPENED = :opened
  STATE_ASSIGNED = :assigned

  acts_as_commentable
  acts_as_paranoid

  belongs_to :user
  belongs_to :family
  belongs_to :category, optional: true

  has_one :user_homework

  scope :ordered_by_name, -> { order(name: :asc) }

  auto_strip_attributes :name, :description

  validates :name, :description, :user_id, :family_id, presence: true

  aasm(column: :state) do
    state :opened, initial: true
    state :assigned, :started, :paused, :ended, :closed

    event :assign do
      transitions from: :opened, to: :assigned
    end

    event :start do
      transitions to: :started
    end

    event :pause do
      transitions from: :started, to: :paused
    end

    event :end do
      transitions from: %i[started paused], to: :ended, after: -> do
        self.ended_at = Time.now.utc
        save
      end
    end

    event :close do
      transitions from: :ended, to: :closed,
        after: -> { update_users_homeworks_counters }
    end

    event :move_to_opened do
      transitions to: :opened, after: -> { user_homework&.destroy }
    end
  end

  private

  def update_users_homeworks_counters
    worker = user_homework.user
    worker.increment!(:completed_homeworks_count)
    if user_homework.estimate >= ended_at
      worker.increment!(:completed_homeworks_on_time_count)
    else
      worker.increment!(:overdue_homeworks_count)
    end
  end
end
