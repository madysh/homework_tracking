class Category < ApplicationRecord
  include Scopable

  MAX_LEVEL = 3

  acts_as_paranoid
  has_ltree_hierarchy

  scope :ordered_by_name, -> { order(name: :asc) }

  belongs_to :family
  belongs_to :parent,
    class_name: 'Category',
    foreign_key: :parent_id,
    optional: true
  has_many :homeworks, dependent: :destroy

  auto_strip_attributes :name

  validates :name, :family_id, presence: true
end
