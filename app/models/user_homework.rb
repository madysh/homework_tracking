class UserHomework < ApplicationRecord
  acts_as_paranoid

  belongs_to :user
  belongs_to :homework
  belongs_to :assigned_by_user,
    foreign_key: :assigned_by_id,
    class_name: 'User'
  has_one :rating

  validates_uniqueness_of :homework_id

  def clear_estimate
    self.estimate = nil
    save
  end
end
