class AddEndedAtToHomework < ActiveRecord::Migration[5.1]
  def change
    add_column :homeworks, :ended_at, :date
  end
end
