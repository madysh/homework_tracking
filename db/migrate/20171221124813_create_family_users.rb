class CreateFamilyUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :family_users do |t|
      t.references :user, null: false, index: true
      t.references :family, null: false, index: true
      t.integer :role, null: false, limit: 1
      t.timestamps
    end
  end
end
