class AddIndexFamilyIdRoleToFamilyUsers < ActiveRecord::Migration[5.1]
  def change
    add_index :family_users, %i[family_id role]
  end
end
