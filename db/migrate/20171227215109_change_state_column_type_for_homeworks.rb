class ChangeStateColumnTypeForHomeworks < ActiveRecord::Migration[5.1]
  def up
    remove_column :homeworks, :state
    add_column :homeworks, :state, :string,
      index: true, null: false, default: :opened
  end

  def down
    remove_column :homeworks, :state
    add_column :homeworks, :state, :integer, default: 0, null: false
  end
end
