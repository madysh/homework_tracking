class AddDeletedAtToHomework < ActiveRecord::Migration[5.1]
  def change
    add_column :homeworks, :deleted_at, :datetime
    add_index :homeworks, :deleted_at
  end
end
