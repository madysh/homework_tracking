class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.references :family
      t.references :category, null: true
      t.string :name, null: false
      t.integer :scope, default: 0, null: false
      t.timestamps
    end
  end
end
