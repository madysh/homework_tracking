class CreateUserHomeworks < ActiveRecord::Migration[5.1]
  def change
    create_table :user_homeworks do |t|
      t.references :user
      t.references :homework
      t.references :assigned_by, as: :user
      t.datetime :estimate
      t.datetime :closed_at
      t.timestamps
    end
  end
end
