class AddPathAndParentIdToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :path, :ltree, null: true
    add_column :categories, :parent_id, :integer

    add_index :categories, :parent_id
  end
end
