class RemoveLavelAndCategoryIdFromCategories < ActiveRecord::Migration[5.1]
  def up
    remove_column :categories, :level
    remove_column :categories, :category_id
  end

  def down
    add_column :categories, :level, :integer, null: false, default: 0
    add_column :categories, :category_id, :integer
  end
end
