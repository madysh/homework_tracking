class AddCountersColumnsToUsers < ActiveRecord::Migration[5.1]
  def change
    columns = %i[
      completed_homeworks_count
      completed_homeworks_on_time_count
      overdue_homeworks_count
      ]
    columns.each do |column_name|
      add_column :users, column_name, :integer, default: 0, null: false
    end
  end
end
