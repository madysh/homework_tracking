class CreateHomeworks < ActiveRecord::Migration[5.1]
  def change
    create_table :homeworks do |t|
      t.references :user
      t.references :family
      t.references :category, null: true
      t.string :name, null: false
      t.text :description, null: false
      t.integer :state, default: 0, null: false, index: true
      t.integer :scope, default: 0, null: false, index: true
      t.timestamps
    end
  end
end
