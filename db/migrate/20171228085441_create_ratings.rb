class CreateRatings < ActiveRecord::Migration[5.1]
  def change
    create_table :ratings do |t|
      t.references :user_homework
      t.integer :score, default: 0, null: false
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
