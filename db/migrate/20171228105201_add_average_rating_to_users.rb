class AddAverageRatingToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :average_rating, :integer, default: 0
  end
end
