class AddDeletedAtToUserHomework < ActiveRecord::Migration[5.1]
  def change
    add_column :user_homeworks, :deleted_at, :datetime
    add_index :user_homeworks, :deleted_at
  end
end
