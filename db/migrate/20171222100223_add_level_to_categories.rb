class AddLevelToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :level, :integer, limit: 1, default: 0
  end
end
