class CreateFamilies < ActiveRecord::Migration[5.1]
  def change
    create_table :families do |t|
      t.string :name, null: false
      t.integer :owner_id, null: false, index: true
      t.string :logo
      t.timestamps
    end
  end
end
