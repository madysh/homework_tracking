# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171228105201) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "ltree"

  create_table "categories", force: :cascade do |t|
    t.bigint "family_id"
    t.string "name", null: false
    t.integer "scope", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.ltree "path"
    t.integer "parent_id"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_categories_on_deleted_at"
    t.index ["family_id"], name: "index_categories_on_family_id"
    t.index ["parent_id"], name: "index_categories_on_parent_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "body"
    t.string "commentable_type"
    t.bigint "commentable_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_id"], name: "index_comments_on_commentable_id"
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
    t.index ["commentable_type"], name: "index_comments_on_commentable_type"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "families", force: :cascade do |t|
    t.string "name", null: false
    t.integer "owner_id", null: false
    t.string "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_families_on_deleted_at"
    t.index ["owner_id"], name: "index_families_on_owner_id"
  end

  create_table "family_users", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "family_id", null: false
    t.integer "role", limit: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["family_id", "role"], name: "index_family_users_on_family_id_and_role"
    t.index ["family_id"], name: "index_family_users_on_family_id"
    t.index ["user_id"], name: "index_family_users_on_user_id"
  end

  create_table "homeworks", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "family_id"
    t.bigint "category_id"
    t.string "name", null: false
    t.text "description", null: false
    t.integer "scope", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "state", default: "opened", null: false
    t.date "ended_at"
    t.index ["category_id"], name: "index_homeworks_on_category_id"
    t.index ["deleted_at"], name: "index_homeworks_on_deleted_at"
    t.index ["family_id"], name: "index_homeworks_on_family_id"
    t.index ["scope"], name: "index_homeworks_on_scope"
    t.index ["user_id"], name: "index_homeworks_on_user_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.bigint "user_homework_id"
    t.integer "score", default: 0, null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_homework_id"], name: "index_ratings_on_user_homework_id"
  end

  create_table "user_homeworks", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "homework_id"
    t.bigint "assigned_by_id"
    t.datetime "estimate"
    t.datetime "closed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["assigned_by_id"], name: "index_user_homeworks_on_assigned_by_id"
    t.index ["deleted_at"], name: "index_user_homeworks_on_deleted_at"
    t.index ["homework_id"], name: "index_user_homeworks_on_homework_id"
    t.index ["user_id"], name: "index_user_homeworks_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", default: "", null: false
    t.string "last_name"
    t.string "nickname"
    t.date "date_of_birth"
    t.string "address"
    t.string "avatar"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "system_language", limit: 2, default: 0, null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "completed_homeworks_count", default: 0, null: false
    t.integer "completed_homeworks_on_time_count", default: 0, null: false
    t.integer "overdue_homeworks_count", default: 0, null: false
    t.integer "average_rating", default: 0
    t.index ["deleted_at"], name: "index_users_on_deleted_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
