source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'aasm'
gem 'acts_as_commentable'
gem 'auto_strip_attributes'
gem 'bootstrap-sass'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.47'
gem 'carrierwave', '~> 1.0'
gem 'coffee-rails', '~> 4.2'
gem 'devise', git: 'https://github.com/plataformatec/devise' #, ref: '88e9a85'
gem 'draper'
gem 'elasticsearch-model'
gem 'elasticsearch-rails'
gem 'haml-rails'
gem 'jquery-rails'
gem 'ltree_hierarchy'
gem 'mini_magick'
gem "paranoia", '~> 2.2'
gem 'pg', '~> 0.18'
gem 'popper_js', '~> 1.12.9'
gem 'puma', '~> 3.7'
gem 'pundit'
gem 'rails', '~> 5.1.4'
gem 'sass-rails', '~> 5.0'
gem 'simple_form'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'selenium-webdriver'
end

group :development, :test do
  gem 'factory_bot_rails', '~> 4.0'
  gem 'pry'
  gem 'rspec-rails'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'shoulda-matchers', '~> 3.1'
end
