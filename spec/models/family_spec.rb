require 'rails_helper'

RSpec.describe Family, type: :model do
  context 'validations' do
    describe '#name' do
      it { is_expected.to validate_presence_of(:name) }
    end

    describe '#owner' do
      it { is_expected.to validate_presence_of(:owner_id) }
    end
  end

  context 'associations' do
    it { is_expected.to have_many(:family_users).dependent(:destroy) }
    it { is_expected.to have_many(:categories).dependent(:destroy) }
    it do
      is_expected
        .to have_many(:users)
        .through(:family_users)
        .dependent(:destroy)
    end
  end
end
