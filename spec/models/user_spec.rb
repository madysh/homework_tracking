require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validations' do
    describe '#password' do
      it { is_expected.to validate_presence_of(:password) }
      it do
        is_expected
          .to validate_length_of(:password)
          .is_at_least(6)
          .is_at_most(128)
      end
    end

    describe '#first_name' do
      it { is_expected.to validate_presence_of(:first_name) }
    end

    describe '#nickname' do
      it do
        is_expected
          .to validate_uniqueness_of(:nickname)
          .ignoring_case_sensitivity
      end
    end
    describe '#email' do
      it do
        is_expected
          .to validate_uniqueness_of(:email)
          .ignoring_case_sensitivity
      end
      it 'allows email format values' do
        valid_values = %w[user@domain.com -user1-@domain.com]
        is_expected.to allow_values(*valid_values).for(:email)
      end
      it 'does not allow invalid values' do
        invalid_values = %w[
          @@domain.com @domain.com user@com domain.com domain
        ]
        is_expected.to_not allow_values(*invalid_values).for(:email)
      end
    end
  end

  context 'associations' do
    it { is_expected.to have_many(:family_users).dependent(:destroy) }
    it do
      is_expected
        .to have_many(:families)
        .through(:family_users)
        .dependent(:destroy)
    end
  end
end
