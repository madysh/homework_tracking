require 'rails_helper'

RSpec.describe Homework, type: :model do
  context 'validations' do
    %i[name description user_id family_id].each do |attr|
      describe "##{attr}" do
        it { is_expected.to validate_presence_of(attr) }
      end
    end
  end

  context 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:family) }
    it { is_expected.to belong_to(:category) }
  end
end
