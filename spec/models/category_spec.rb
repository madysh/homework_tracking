require 'rails_helper'

RSpec.describe Category, type: :model do
  context 'validations' do
    %i[name family_id].each do |attr|
      describe "##{attr}" do
        it { is_expected.to validate_presence_of(attr) }
      end
    end

    describe '#scope' do
      it do
        is_expected
          .to define_enum_for(:scope)
          .with(%i[as_public as_private])
      end
    end
  end

  context 'associations' do
    it { is_expected.to belong_to(:family) }
    it { is_expected.to belong_to(:parent) }
  end
end
