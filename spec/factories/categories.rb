FactoryBot.define do
  factory :category do
    sequence(:name) { |n| "category-#{n}" }
    family
    scope :as_public
  end
end
